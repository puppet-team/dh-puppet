# dh-puppet

The dh-puppet package contains a `puppet-module` debhelper sequence that aims
to simplify the packaging of [Puppet][] modules.

It will handle installing the module files directories such as `data`,
`manifests`, `templates` and others, documentation and maintscripts for
integration with the alternatives mechanism.

In addition, it will generate the `${puppet_module:Depends}` substitution
variable containing the module dependencies derived from `metadata.json`.

To activate it, just add `dh-sequence-puppet-module` to Build-Depends.

[Puppet]: https://www.puppet.com/
